a couple of url module extensions

url.resolveAny

needs some thought and testing.
resolver for koa/express-style parametrized urls
url.resolve doesn't preserve regex slashes so do it manually
by replacing all except '/' with '*', resolving with url.resolve
and then going back to original values. Otherwise should be acting the same.

url.join - same as path.join for urls, eats up consecutive slashes: a///b -> a/b

url.fromRoute - Reconstructs url from express/koa route with named parameters:

    url.fromRoute("/post/:id(\d+)/delete/", { id : 1100 }) == "/post/1100/delete/"

if values is a primitive it becomes an array and
if values is an array parameters in the url are replaced sequentially with array values.





