var url = require('url');

// url.resolve doesn't preserve regex slashes so do it manually
// by replacing all except '/' with '*', resolving with url.resolve
// and then going back to original values
url.resolveAny = function resolveAny(fromUrl, relativePath)
{
	if(!relativePath) return fromUrl;
	var res = url
			.resolve(fromUrl.replace(/[^/]/g,'*'), relativePath)
			.split('')
			.map(function(c,i) { return c == '/' ? c : fromUrl[i] })
			.join('')
			.replace(/\/+/, "\/");

	return res;
}

// path.join for urls
url.join = function urlJoin() {
	// from https://github.com/jfromaniello/url-join/blob/master/lib/url-join.js
    var joined = [].slice.call(arguments, 0).join('/');
    return joined.replace(/[\/]+/g, '/').replace(/\/\?/g, '?').replace(/\/\#/g, '#').replace(/\:\//g, '://');
}

// Reconstructs url from express/koa route with named parameters:
// 
// url.fromRoute("/post/:id(\d+)/delete/", { id : 1100 }) == "/post/1100/delete/"
//
// if values is a primitive it becomes an array and
// if values is an array parameters in the url are replaced sequentially with array values.
url.fromRoute = function(route, values) {
	if(typeof values !== 'object' && !(values instanceof Array))
		values = [values];
	if(!(values instanceof Array))
		for(var v in values)
			route = route.replace(new RegExp("(:" + v + "\\(.+?\\))"), values[v]);
	else
		for(var v of values)
			route = route.replace(/((:.+?)?\(.+?\))/, v)

	return route;	
}

module.exports = url;
